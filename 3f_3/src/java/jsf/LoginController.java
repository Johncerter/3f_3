/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
//import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import jpa.session.UsersFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Kike
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
    }
    private String username;
    private String password;
    private boolean loggedIn;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    @EJB
    private jpa.session.UsersFacade ejbFacade;

    public UsersFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(UsersFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public String loginControl() {
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext.getCurrentInstance().update("growl");
        if (getEjbFacade().connect(username, password)) {
            this.setUsername(username);
            context.getExternalContext().getSessionMap().put("user", username);
            loggedIn = true;
            return "index_1";
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error de acceso"));
        return "";
    }

    public void logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
        try {
            context.getExternalContext().redirect("index.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
