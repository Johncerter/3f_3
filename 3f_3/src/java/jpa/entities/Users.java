/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    , @NamedQuery(name = "Users.findByRutUsers", query = "SELECT u FROM Users u WHERE u.rutUsers = :rutUsers")
        , @NamedQuery(name="Users.control", query="SELECT l FROM Users l WHERE l.username=:username AND l.password=:password")
    , @NamedQuery(name = "Users.findByNombreUsuario", query = "SELECT u FROM Users u WHERE u.nombreUsuario = :nombreUsuario")
    , @NamedQuery(name = "Users.findByApellidoUsuario", query = "SELECT u FROM Users u WHERE u.apellidoUsuario = :apellidoUsuario")
    , @NamedQuery(name = "Users.findByDireccionUsuario", query = "SELECT u FROM Users u WHERE u.direccionUsuario = :direccionUsuario")
    , @NamedQuery(name = "Users.findByTelefonoUsuario", query = "SELECT u FROM Users u WHERE u.telefonoUsuario = :telefonoUsuario")
    , @NamedQuery(name = "Users.findByCorreoUsuario", query = "SELECT u FROM Users u WHERE u.correoUsuario = :correoUsuario")
    , @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username")
    , @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "rut_users")
    private String rutUsers;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre_usuario")
    private String nombreUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellido_usuario")
    private String apellidoUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "direccion_usuario")
    private String direccionUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telefono_usuario")
    private int telefonoUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "correo_usuario")
    private String correoUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "password")
    private String password;
    @JoinColumn(name = "id_tipo", referencedColumnName = "id_tipo")
    @ManyToOne(optional = false)
    private TipoUser idTipo;

    public Users() {
    }

    public Users(String rutUsers) {
        this.rutUsers = rutUsers;
    }

    public Users(String rutUsers, String nombreUsuario, String apellidoUsuario, String direccionUsuario, int telefonoUsuario, String correoUsuario, String username, String password) {
        this.rutUsers = rutUsers;
        this.nombreUsuario = nombreUsuario;
        this.apellidoUsuario = apellidoUsuario;
        this.direccionUsuario = direccionUsuario;
        this.telefonoUsuario = telefonoUsuario;
        this.correoUsuario = correoUsuario;
        this.username = username;
        this.password = password;
    }

    public String getRutUsers() {
        return rutUsers;
    }

    public void setRutUsers(String rutUsers) {
        this.rutUsers = rutUsers;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public String getDireccionUsuario() {
        return direccionUsuario;
    }

    public void setDireccionUsuario(String direccionUsuario) {
        this.direccionUsuario = direccionUsuario;
    }

    public int getTelefonoUsuario() {
        return telefonoUsuario;
    }

    public void setTelefonoUsuario(int telefonoUsuario) {
        this.telefonoUsuario = telefonoUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TipoUser getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(TipoUser idTipo) {
        this.idTipo = idTipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutUsers != null ? rutUsers.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.rutUsers == null && other.rutUsers != null) || (this.rutUsers != null && !this.rutUsers.equals(other.rutUsers))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Users[ rutUsers=" + rutUsers + " ]";
    }
    
}
