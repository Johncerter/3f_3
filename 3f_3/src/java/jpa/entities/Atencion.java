/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "atencion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Atencion.findAll", query = "SELECT a FROM Atencion a")
    , @NamedQuery(name = "Atencion.findByIdAtencion", query = "SELECT a FROM Atencion a WHERE a.idAtencion = :idAtencion")
    , @NamedQuery(name = "Atencion.findByFechaCreacion", query = "SELECT a FROM Atencion a WHERE a.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "Atencion.findByFechaInicio", query = "SELECT a FROM Atencion a WHERE a.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Atencion.findByFechaTermino", query = "SELECT a FROM Atencion a WHERE a.fechaTermino = :fechaTermino")
    , @NamedQuery(name = "Atencion.findByEstado", query = "SELECT a FROM Atencion a WHERE a.estado = :estado")})
public class Atencion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_atencion")
    private Integer idAtencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_termino")
    @Temporal(TemporalType.DATE)
    private Date fechaTermino;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "id_ficha", referencedColumnName = "id_ficha")
    @ManyToOne(optional = false)
    private FichaDental idFicha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAtencion")
    private List<TratamientoRealizado> tratamientoRealizadoList;

    public Atencion() {
    }

    public Atencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public Atencion(Integer idAtencion, Date fechaCreacion, Date fechaInicio, Date fechaTermino, String estado) {
        this.idAtencion = idAtencion;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        this.fechaTermino = fechaTermino;
        this.estado = estado;
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public FichaDental getIdFicha() {
        return idFicha;
    }

    public void setIdFicha(FichaDental idFicha) {
        this.idFicha = idFicha;
    }

    @XmlTransient
    public List<TratamientoRealizado> getTratamientoRealizadoList() {
        return tratamientoRealizadoList;
    }

    public void setTratamientoRealizadoList(List<TratamientoRealizado> tratamientoRealizadoList) {
        this.tratamientoRealizadoList = tratamientoRealizadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtencion != null ? idAtencion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Atencion)) {
            return false;
        }
        Atencion other = (Atencion) object;
        if ((this.idAtencion == null && other.idAtencion != null) || (this.idAtencion != null && !this.idAtencion.equals(other.idAtencion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + idAtencion + "";
    }
    
}
