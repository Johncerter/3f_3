/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p")
    , @NamedQuery(name = "Paciente.findByIdPaciente", query = "SELECT p FROM Paciente p WHERE p.idPaciente = :idPaciente")
    , @NamedQuery(name = "Paciente.findByNombrePaciente", query = "SELECT p FROM Paciente p WHERE p.nombrePaciente = :nombrePaciente")
    , @NamedQuery(name = "Paciente.findByApellidoPaciente", query = "SELECT p FROM Paciente p WHERE p.apellidoPaciente = :apellidoPaciente")
    , @NamedQuery(name = "Paciente.findByTelefonoPaciente", query = "SELECT p FROM Paciente p WHERE p.telefonoPaciente = :telefonoPaciente")
    , @NamedQuery(name = "Paciente.findByCorreoPaciente", query = "SELECT p FROM Paciente p WHERE p.correoPaciente = :correoPaciente")
    , @NamedQuery(name = "Paciente.findBySexoPaciente", query = "SELECT p FROM Paciente p WHERE p.sexoPaciente = :sexoPaciente")
    , @NamedQuery(name = "Paciente.findByFechaNacPaciente", query = "SELECT p FROM Paciente p WHERE p.fechaNacPaciente = :fechaNacPaciente")})
public class Paciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_paciente")
    private Integer idPaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre_paciente")
    private String nombrePaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellido_paciente")
    private String apellidoPaciente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telefono_paciente")
    private int telefonoPaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "correo_paciente")
    private String correoPaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "sexo_paciente")
    private String sexoPaciente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_nac_paciente")
    @Temporal(TemporalType.DATE)
    private Date fechaNacPaciente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacienteIdPaciente")
    private List<FichaDental> fichaDentalList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacienteIdPaciente")
    private List<Cita> citaList;

    public Paciente() {
    }

    public Paciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public Paciente(Integer idPaciente, String nombrePaciente, String apellidoPaciente, int telefonoPaciente, String correoPaciente, String sexoPaciente, Date fechaNacPaciente) {
        this.idPaciente = idPaciente;
        this.nombrePaciente = nombrePaciente;
        this.apellidoPaciente = apellidoPaciente;
        this.telefonoPaciente = telefonoPaciente;
        this.correoPaciente = correoPaciente;
        this.sexoPaciente = sexoPaciente;
        this.fechaNacPaciente = fechaNacPaciente;
    }

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getApellidoPaciente() {
        return apellidoPaciente;
    }

    public void setApellidoPaciente(String apellidoPaciente) {
        this.apellidoPaciente = apellidoPaciente;
    }

    public int getTelefonoPaciente() {
        return telefonoPaciente;
    }

    public void setTelefonoPaciente(int telefonoPaciente) {
        this.telefonoPaciente = telefonoPaciente;
    }

    public String getCorreoPaciente() {
        return correoPaciente;
    }

    public void setCorreoPaciente(String correoPaciente) {
        this.correoPaciente = correoPaciente;
    }

    public String getSexoPaciente() {
        return sexoPaciente;
    }

    public void setSexoPaciente(String sexoPaciente) {
        this.sexoPaciente = sexoPaciente;
    }

    public Date getFechaNacPaciente() {
        return fechaNacPaciente;
    }

    public void setFechaNacPaciente(Date fechaNacPaciente) {
        this.fechaNacPaciente = fechaNacPaciente;
    }

    @XmlTransient
    public List<FichaDental> getFichaDentalList() {
        return fichaDentalList;
    }

    public void setFichaDentalList(List<FichaDental> fichaDentalList) {
        this.fichaDentalList = fichaDentalList;
    }

    @XmlTransient
    public List<Cita> getCitaList() {
        return citaList;
    }

    public void setCitaList(List<Cita> citaList) {
        this.citaList = citaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPaciente != null ? idPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.idPaciente == null && other.idPaciente != null) || (this.idPaciente != null && !this.idPaciente.equals(other.idPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + idPaciente + "";
    }
    
}
