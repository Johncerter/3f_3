/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "tratamiento_realizado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TratamientoRealizado.findAll", query = "SELECT t FROM TratamientoRealizado t")
    , @NamedQuery(name = "TratamientoRealizado.findByIdRealizado", query = "SELECT t FROM TratamientoRealizado t WHERE t.idRealizado = :idRealizado")})
public class TratamientoRealizado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_realizado")
    private Integer idRealizado;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "comentario")
    private String comentario;
    @JoinColumn(name = "id_atencion", referencedColumnName = "id_atencion")
    @ManyToOne(optional = false)
    private Atencion idAtencion;
    @JoinColumn(name = "id_tratamiento", referencedColumnName = "id_tratamiento")
    @ManyToOne(optional = false)
    private Tratamiento idTratamiento;

    public TratamientoRealizado() {
    }

    public TratamientoRealizado(Integer idRealizado) {
        this.idRealizado = idRealizado;
    }

    public TratamientoRealizado(Integer idRealizado, String comentario) {
        this.idRealizado = idRealizado;
        this.comentario = comentario;
    }

    public Integer getIdRealizado() {
        return idRealizado;
    }

    public void setIdRealizado(Integer idRealizado) {
        this.idRealizado = idRealizado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Atencion getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Atencion idAtencion) {
        this.idAtencion = idAtencion;
    }

    public Tratamiento getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(Tratamiento idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRealizado != null ? idRealizado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TratamientoRealizado)) {
            return false;
        }
        TratamientoRealizado other = (TratamientoRealizado) object;
        if ((this.idRealizado == null && other.idRealizado != null) || (this.idRealizado != null && !this.idRealizado.equals(other.idRealizado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.TratamientoRealizado[ idRealizado=" + idRealizado + " ]";
    }
    
}
