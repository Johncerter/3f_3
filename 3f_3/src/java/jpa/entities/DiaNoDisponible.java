/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "dia_no_disponible")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DiaNoDisponible.findAll", query = "SELECT d FROM DiaNoDisponible d")
    , @NamedQuery(name = "DiaNoDisponible.findByIdDiaNoDisponible", query = "SELECT d FROM DiaNoDisponible d WHERE d.idDiaNoDisponible = :idDiaNoDisponible")
    , @NamedQuery(name = "DiaNoDisponible.findByFecha", query = "SELECT d FROM DiaNoDisponible d WHERE d.fecha = :fecha")})
public class DiaNoDisponible implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_dia_no_disponible")
    private Integer idDiaNoDisponible;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "id_dia", referencedColumnName = "id_dia")
    @ManyToOne(optional = false)
    private Dia idDia;

    public DiaNoDisponible() {
    }

    public DiaNoDisponible(Integer idDiaNoDisponible) {
        this.idDiaNoDisponible = idDiaNoDisponible;
    }

    public DiaNoDisponible(Integer idDiaNoDisponible, Date fecha) {
        this.idDiaNoDisponible = idDiaNoDisponible;
        this.fecha = fecha;
    }

    public Integer getIdDiaNoDisponible() {
        return idDiaNoDisponible;
    }

    public void setIdDiaNoDisponible(Integer idDiaNoDisponible) {
        this.idDiaNoDisponible = idDiaNoDisponible;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Dia getIdDia() {
        return idDia;
    }

    public void setIdDia(Dia idDia) {
        this.idDia = idDia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDiaNoDisponible != null ? idDiaNoDisponible.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiaNoDisponible)) {
            return false;
        }
        DiaNoDisponible other = (DiaNoDisponible) object;
        if ((this.idDiaNoDisponible == null && other.idDiaNoDisponible != null) || (this.idDiaNoDisponible != null && !this.idDiaNoDisponible.equals(other.idDiaNoDisponible))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.DiaNoDisponible[ idDiaNoDisponible=" + idDiaNoDisponible + " ]";
    }
    
}
