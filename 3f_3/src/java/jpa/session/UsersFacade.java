/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Users;

/**
 *
 * @author ASUS
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> {
public Users connect = null;

    @PersistenceContext(unitName = "3f_3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

     public boolean connect(String username, String password) {
        try {
            connect = this.getEntityManager().createNamedQuery("Users.control", Users.class).setParameter("username", username).setParameter("password", password).getSingleResult();
            if(connect != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    
    public UsersFacade() {
        super(Users.class);
    }
    
}
