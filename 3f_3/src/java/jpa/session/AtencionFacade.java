/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Atencion;

/**
 *
 * @author ASUS
 */
@Stateless
public class AtencionFacade extends AbstractFacade<Atencion> {

    @PersistenceContext(unitName = "3f_3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AtencionFacade() {
        super(Atencion.class);
    }
    
}
