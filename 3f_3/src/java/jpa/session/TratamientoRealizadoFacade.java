/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.TratamientoRealizado;

/**
 *
 * @author ASUS
 */
@Stateless
public class TratamientoRealizadoFacade extends AbstractFacade<TratamientoRealizado> {

    @PersistenceContext(unitName = "3f_3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TratamientoRealizadoFacade() {
        super(TratamientoRealizado.class);
    }
    
}
